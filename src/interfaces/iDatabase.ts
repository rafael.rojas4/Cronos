import { QueryResult } from "pg";

export default interface IDatabase {
	makeQuery(query: string, params: any[]): Promise<QueryResult>;
	closeConnection(): Promise<void>;
}