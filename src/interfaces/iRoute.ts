import { Router } from "express";
import IDatabase from "./iDatabase";

export default interface IRoute {
	get route(): Router;
	setDatabase(dbConnection: IDatabase): void;
	initRoutes(): void;
}