import { Request, Response } from "express";
import IDatabase from "../interfaces/iDatabase";
// import ActorService from "../services/actorService";
import FilmService from "../services/filmService";

export default class FilmController {
	private _dbConnection: IDatabase;
	private _filmService: FilmService;

	constructor(dbConnection: IDatabase) {
		this._dbConnection = dbConnection;
		this._filmService = new FilmService(this._dbConnection);
	}

	async getFilms(req: Request, res: Response) {
		try {
			let result = await this._filmService.getFilms();
			res.send(result);
		} catch (error) {
			res.status(400).json(error);
		}
	}

	async getFilm(req: Request, res: Response) {
		try {
			let id = parseInt(req.params.id);
			let result = await this._filmService.getFilmById(id);
			res.send(result);
		} catch (error) {
			res.status(400).json(error);
		}
	}
}