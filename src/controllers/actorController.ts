import { Request, Response } from "express";
import IDatabase from "../interfaces/iDatabase";
import ActorService from "../services/actorService";

export default class ActorController {
	private _dbConnection: IDatabase;
	private _actorService: ActorService;

	constructor(dbConnection: IDatabase) {
		this._dbConnection = dbConnection;
		this._actorService = new ActorService(this._dbConnection);
	}

	async getActors(req: Request, res: Response) {
		try {
			let result = await this._actorService.getActorByLastName('DAVIS');
			res.send(result);
		} catch (error) {
			res.status(400).json(error);
		}
	}

	async getActor(req: Request, res: Response) {
		try {
			let id = parseInt(req.params.id);
			let result = await this._actorService.getActorById(id);
			res.send(result);
		} catch (error) {
			res.status(400).json(error);
		}
	}
}