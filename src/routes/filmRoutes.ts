import { Router } from "express";
import FilmController from "../controllers/filmController";
import IDatabase from "../interfaces/iDatabase";
import IRoute from "../interfaces/iRoute";

export default class FilmRoute implements IRoute {
	private _router: Router;
	private _filmController?: FilmController;

	constructor() {
		this._router = Router();
	}

	get route(): Router {
		return this._router;
	}

	setDatabase(dbConnection: IDatabase): void {
		this._filmController = new FilmController(dbConnection);
	}

	initRoutes(): void {
		if (this._filmController) {
			this._router.get('/films', this._filmController.getFilms.bind(this._filmController));
			this._router.get('/films/:id', this._filmController.getFilm.bind(this._filmController));
		}
	}
}