import { Router } from "express";
import ActorController from "../controllers/actorController";
import IDatabase from "../interfaces/iDatabase";
import IRoute from "../interfaces/iRoute";

export default class ActorRoute implements IRoute {
	private _router: Router;
	private _actorController?: ActorController;

	constructor() {
		this._router = Router();
	}

	get route(): Router {
		return this._router;
	}

	setDatabase(dbConnection: IDatabase): void {
		this._actorController = new ActorController(dbConnection);
	}

	initRoutes(): void {
		if (this._actorController) {
			this._router.get('/actors', this._actorController.getActors.bind(this._actorController));
			this._router.get('/actors/:id', this._actorController.getActor.bind(this._actorController));
		}
	}
}