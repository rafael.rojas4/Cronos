import express, { Application } from 'express';
import 'dotenv/config';
import DbPostgres from './database/dbPostgres';
import IDatabase from './interfaces/iDatabase';
import IRoute from './interfaces/iRoute';

export default class App {
	private _app: Application;
	private _dbConnection: IDatabase;

	constructor() {
		this._app = express();
		this._app.use(express.json());
		this._app.use(express.urlencoded({extended: false}));

		this._dbConnection = new DbPostgres();
	}

	initServer(): void {
		this._app.listen(process.env.APP_PORT, function () {
			console.log('Cronos service listening on port 3000!');
		});
	}

	addRoute(route: IRoute): void {
		route.setDatabase(this._dbConnection);
		route.initRoutes();
		this._app.use(route.route);
	}

	// addRouteWithMiddleware(route: IRoute, middleware): void {
	// 	this._app.use(middleware, route.route);
	// }
}