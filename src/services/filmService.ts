import IDatabase from "../interfaces/iDatabase";
import Film from "../models/film";

export default class FilmService {
	private _dbConection: IDatabase;

	constructor(dbConnection: IDatabase) {
		this._dbConection = dbConnection;
	}

	async getFilms() {
		const query = "select * from film";
		let result = await this._dbConection.makeQuery(query, []);
		return result.rows;
	}

	async getFilmById(filmId: number) {
		const query = "select * from film f where film_id = $1";
		let data = await this._dbConection.makeQuery(query, [filmId]);
		let film: Film = data.rows[0];
		return film;
	}
}