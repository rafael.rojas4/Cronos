import IDatabase from "../interfaces/iDatabase";
import Actor from "../models/actor";

export default class ActorService {
	private _dbConection: IDatabase;

	constructor(dbConnection: IDatabase) {
		this._dbConection = dbConnection;
	}

	async getActorByLastName(lastName: string) {
		const query = "select * from actor a where last_name = $1";
		let result = await this._dbConection.makeQuery(query, [lastName]);
		return result.rows;
	}

	async getActorById(actorId: number): Promise<Actor> {
		const query = "select * from actor a where actor_id = $1";
		let data = await this._dbConection.makeQuery(query, [actorId]);
		let result: Actor = data.rows[0];
		return result;
	}
}