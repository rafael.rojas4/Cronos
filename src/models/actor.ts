export default class Actor {
	actor_id: number = 0;
	first_name: string = '';
	last_name: string = '';
	last_update: Date = new Date();
}