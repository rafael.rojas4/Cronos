import App from "./app";
import ActorRoute from "./routes/actorRoutes";
import FilmRoute from "./routes/filmRoutes";

const app = new App();
app.initServer();

// adding all the routes
app.addRoute(new ActorRoute());
app.addRoute(new FilmRoute());