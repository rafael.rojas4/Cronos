import { QueryResult, Pool } from 'pg';
import IDatabase from '../interfaces/iDatabase';

export default class DbPostgres implements IDatabase {
	private readonly connectionData = {
		user: process.env.DB_USER,
		host: process.env.DB_HOST,
		database: process.env.DB_DATABASE,
		password: process.env.DB_PASSWORD,
		port: process.env.DB_PORT as number|undefined,
	}

	private pool: Pool;

	constructor() {
		this.pool = new Pool(this.connectionData);
		this.pool.connect();
	}

	makeQuery(query: string, params:any[]): Promise<QueryResult> {
		return this.pool.query(query, params);
	}

	async closeConnection() {
		await this.pool.end();
	}
}