lo necesario para iniciar:

-crear una copia del archivo .env.example
-renombrar la copia a .env
- aniadir los datos de conneccion de la BD, los datos por defecto lo estoy dejando para conectarse a la BD sakila que manejamos en docker
	APP_PORT=3000 <- elpuerto en el que correra la aplicacion en localhost
	DB_USER=sakila
	DB_HOST=localhost
	DB_DATABASE=sakila
	DB_PASSWORD=p_ssW0rd
	DB_PORT=5432

se puede iniciar la aplicacion de 2 maneras
primera manera de iniciar:
- 'npm install' para crear node_modules 
- ejecutar la aplicacion en modo desarrollo con 
	npm run dev

segunda manera de iniciar:
-npm run tsc
despues de ejecutar ese script se creara la carpeta build
despues ejecutar 'npm run start'

-la aplicacion correra en http://localhost:3000
- si se estan conectando a sakila pueden probar los apis que cree para actor y film:
	- La siguiente direccion se mostrara en el navegador los actores con apellido 'DAVIS'
	http://localhost:3000/actors

	- La siguiente direccion les entregara los datos de un actor por si id
	http://localhost:3000/actors/2

	- La siguiente direccion les entregara todos los films
	http://localhost:3000/films

	-La siguiente direccion les entregara un film por su id
	http://localhost:3000/films/5

Como esta estructurado
index.ts es el punto de inicio 
este creara una instancia de la clase App en el archivo app.ts

index pasara a app las rutas creadas y en la lgica de app por el uso de la interfaz IRoute los agregara a express.

actorRoutes define sus rutas en el metodo initRoutes, de igual manera filmRoutes.

al definir las rutas se pasa el controller a la ruta especificandole el metodo que se ejecutara con esa ruta, haciendo un bind con la instancia del controlador. 
	this._router.get('/actors', this._actorController.getActors.bind(this._actorController));


en la carpeta controllers encontraran los controladores, los controladores lo unico que hacen es llamar al metodo necesario de los servicios correspondientes.

en los servicios es donde se hara la logica para mandar la querya la BD,
'pg' tiene una forma especifica de recibir las consultas,
tiene un metodo 'query' que recibe la consulta como un string y los datos de la consulta en un array
ej:
	query = 'Select * from actor where actor_id=$1 and first_name=$2';
	params = [2, 'SCARLET'];
	query(query, params)

los parametros deben especificarse en el query con '$1', '$2', '$3', etc.
esos datos se deben agregar en un array;
datos dinamicos solo los acepta asi, trate de pasarle de la siguiente manera:
	- `Select * from actor where actor_id=${id} and first_name=${name}`
	- 'Select * from actor where actor_id=' + id + 'and first_name=' + $2'
y de las dos maneras falla. es cosa de pg.